import java.util.Scanner;

public class Main
{
    /**
     * Classic Tuple object (generic).
     */
    public static class Tuple<X, Y>
    {
        public final X x;
        public final Y y;

        public Tuple(X x, Y y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args)
    {
        System.out.println("Heyho, enter CHF pls:");
        Scanner scan = new Scanner(System.in);

        // Gather the CHF amount from the console.
        double chf = scan.nextDouble();
        if (chf < 0)
        {
            chf *= -1d;
        }

        // Convert to rappen for easier calculation.
        long rappen = (long) (chf * 100);

        // Declare what units the amount should be broken down to (e.g. bill & coin sizes in rappen).
        long[] units = new long[]{100000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5};
        long temp = rappen;
        for (long unit : units)
        {
            // For every unit, get a Tuple containing both the amount and the remainder.
            Tuple<Long, Long> result = getUnits(temp, unit);

            // Store the temporary remainder in a variable declared outside of the loop.
            temp = result.y;

            // Prefix "0." if we're flippin' cents 'n' shit.
            String prefix = (unit < 10) ? "0.0" : (unit < 100) ? "0." : "";

            System.out.println(prefix + (unit < 100 ? unit : unit / 100) + ": " + result.x);
        }

        scan.close();
    }

    /**
     * Get a tuple of the broken down amount and the remainder. All in Rappen.
     * @param amount   The amount of money to break down.
     * @param unitSize The unit to break the amount down to.
     * @return A tuple{int,int} where x is the resulting amount (broken down to the unit) and y the remaining amount.
     */
    static Tuple<Long, Long> getUnits(long amount, long unitSize)
    {
        long units = amount / unitSize;
        amount %= unitSize;
        return new Tuple<>(units, amount);
    }
}
